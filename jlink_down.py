import os
import pylink

def download_program():
    link = pylink.JLink()
    try:
        #连接JLlink设备
        link.open()
        #选择芯片和下载方式
        link.set_tif(pylink.enums.JLinkInterfaces.SWD)
        link.connect('STM32F407ZG')
        #擦除程序
        #link.erase()
        #下载并烧入程序
        link.flash_file("./rtthread.bin",0x08000000)
    finally:
        #关闭与JLink的连接
        link.close()
        
download_program()
