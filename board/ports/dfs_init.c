
#include <rtthread.h>
#include <dfs_fs.h>

#define SPI_FLASH_DEV_NAME "W25Q128"

const struct dfs_mount_tbl mount_table[] =
{
    // {SPI_FLASH_DEV_NAME, "elm", "/", 0, 0},
    {0}
};
/* 挂载文件系统 */
int dfs_mount_init(void)
{
    /* 指定格式化的文件系统，即将w25Q128格式化为ELM文件系统 */
    // 不能每次都调用，只有第一次使用
    // dfs_mkfs("elm", SPI_FLASH_DEV_NAME);

    /* 挂载文件系统 */
    if(dfs_mount(SPI_FLASH_DEV_NAME,"/","elm",0,0) == 0)
    {
        rt_kprintf("dfs_mount success\r\n");
                return RT_EOK;
    }
    else {
        rt_kprintf("dfs_mount fail\r\n");
        return -RT_ERROR;
    }
}
/* 系统启动时自动执行 */
INIT_ENV_EXPORT(dfs_mount_init);   // 初始化执行顺序在注册flash之后
