# STM32F407 跑 AI

## 简介
使用 onnx 组件，跑一个基础的手写识别。

功能：  
- spi + Flash + 文件系统
- led + printf
- onnx 嵌入
- 使用最新的 RT-Thread 5.1.0 版本


## 运行结果

下载程序成功之后，系统会自动运行，观察开发板上 LED 的运行效果，红色 LED 常亮、绿色 LED 会周期性闪烁。

连接开发板对应串口到 PC , 在终端工具里打开相应的串口（115200-8-1-N），复位设备后，可以看到 RT-Thread 的输出信息:

> 注：在使用终端工具如：PuTTy、XShell 时，会出现系统不能启动的问题，推荐使用串口调试助手如：sscom

## 最佳实践

### vscode + clangd + scons/env 配置
1. 下载env开发环境，配置scons
- 下载env工具包 (link)[https://www.rt-thread.org/download.html#download-rt-thread-env-tool]，解压到没有中文路径下。  
- 打开 env.exe, 初始化完成后，找到 conEmu 的settings, 然后找到integration, 集成到系统菜单右键`ConEmu Here`。
- 在初始化好的 venv 环境下，通过 pip 安装`scons-compiledb`。
- 如果初始化时python有问题，建议自己安装配置一套python环境后，再重试。完整pip列表见文件`venv.pip.txt`


2. 安装vscode 和 插件
- 安装 vscode (link)[https://code.visualstudio.com/]
- 安装插件 clangd。(其他可选插件见文件`.vscode/extensions.json`)

3. 制作project
- 克隆RT-Thread对应版本的git代码库
- 找到对应的初始 BSP 包，比如：stm32f407-atk，使用 `CoEmu Here`, 然后`scons --dist --target=vsc --project-name=myai --project-path="D:\mycode"`
- 这样就会生成一个`myai`文件夹，里面就是一个完整的vscode工程。

4. 配置 scons 产生 compile_commands.json
在创建好的rt-thread工程目录下，找到`SConstruct`文件，修改最后一行代码：
```py
# 增加生成 compile_commands.json
import scons_compiledb
scons_compiledb.enable(env)

# make a building
DoBuilding(TARGET, objs)

# 生成 db => compile_commands.json
env.CompileDb()
```
5. 编译
- 在生成好的工程目录下，右键选择 CoEmu Here 命令行，执行`scons`命令，即可生成`compile_commands.json`文件。
- vscode 打开此文件夹即可拥有完整的代码提示。


## note
1. 挂载文件系统参考  
https://blog.csdn.net/lengyuefeng212/article/details/113848520  
https://blog.csdn.net/weixin_46158019/article/details/109643188  

2. 传文件 ymodem  
https://mengplus.top/archives/ymodem  

xmodem,ymodem,zmodem协议区别:  
- Xmodem：这种古老的传输协议速度较慢，但由于使用了CRC错误侦测方法，传输的准确率可高达99.6%。
- Ymodem：这是Xmodem的改良版，使用了1024位区段传送，速度比Xmodem要快。
- Zmodem：采用了串流式（streaming）传输方式，传输速度较快，而且还具有自动改变区段大小和断点续传、快速错误侦测等功能。这是目前最流行的文件传输协议。

```bash
mkdir tmp
ry ./tmp

# 然受使用 SecureCRT 工具, 选择 ymodem 模式, 发送文件

ls ./tmp

# OK
```

3. 每次重启板子，文件系统数据丢失  
因为每次都重新格式化了文件系统，所以每次都要重新传文件。
代码`dfs_mkfs("elm", SPI_FLASH_DEV_NAME);`, 禁用它就好了。
